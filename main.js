let slider = [
  {
    id: 1,
    title: "shemanto",
    text: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda, molestiae..",
  },
  {
    id: 2,
    title: "arafath",
    text: "dolor sit amet consectetur adipisicing elit. Assumenda, molestiae..",
  },
  {
    id: 1,
    title: "arafath shemanto",
    text: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda, molestiae..",
  },
];

let currentItem = 0;
let title = document.getElementById("title");
let text = document.getElementById("text");
window.addEventListener("DOMContentLoaded", function () {
  showPerson();
});
function showPerson() {
  let item = slider[currentItem];
  title.textContent = item.title;
  text.textContent = item.text;
}
// select btns
let prevBtn = document.querySelector(".prev_btn");
let next_btn = document.querySelector(".next_btn");

next_btn.addEventListener("click", function () {
  currentItem++;
  if (currentItem > slider.length - 1) {
    currentItem = 0;
  }

  showPerson();
});
prevBtn.addEventListener("click", function () {
  currentItem--;
  if (currentItem < 0) {
    currentItem = slider.length - 1;
  }

  showPerson(currentItem);
});
